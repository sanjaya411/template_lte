const base_url = "httpL//localhost:8080/";

function sweet(icon, title, text) {
  Swal.fire({
    icon: icon,
    title: title,
    text: text
  })
}

function promptDelete(link) {
  Swal.fire({
    title: 'Anda yakin?',
    text: "Item ini akan dihapus secara permanent!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, Hapus ini!'
  }).then((result) => {
    if (result.value) {
     //  window.location.href = link;
     $.ajax({
       url: link,
       type: "GET",
       cache: false,
       success: function(result) {
         let hasil = JSON.parse(result);
         if (hasil.success == 200) {
           window.location.href = window.location;
         } else {
           Swal.fire({
             title: "Gagal!",
             text: hasil.message,
             icon: "error"
           })
         }
       },
       error: function() {
         Swal.fire({
             title: "Gagal!",
             text: "404 Not Found",
             icon: "error"
           })
       }
     })
    }
  })
}

function onErrorImage(target) {
  target.onerror = null;
  target.src = "../image/user_default.jpg";
}