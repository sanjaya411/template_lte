<?php namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;
use App\Models\Auth_model;


class Auth implements FilterInterface
{
    public function before(RequestInterface $request , $arguments = NULL)
    {
        $auth = new Auth_model();
        if (session()->is_login === TRUE) {
            $check_session = $auth->check_session_token();
            if ($check_session->success === FALSE) {
                session()->setFlashdata("pesan", "<script>sweet('warning', 'Gagal!', '$check_session->message!')</script>");
                return \redirect()->to("/admin");
            }
        } else {
            session()->setFlashdata("pesan", "<script>sweet('error', 'Gagal!', 'Anda wajib melakukan login dahulu!')</script>");
            return \redirect()->to("/admin");
        }
    }

    //--------------------------------------------------------------------

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = NULL)
    {
        
    }
}