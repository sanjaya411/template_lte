<?php

namespace App\Models;

use CodeIgniter\Model;

class Core_model extends Model
{
  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set("Asia/Jakarta");
    \helper("Addon");
    $this->session = \Config\Services::session();
  }

  public function update_session_token()
  {
    if ($this->session->is_login === TRUE) {
      $session = $this->session->token;
      $update = ["expire_time" => date("Y-m-d H:i:s", \strtotime("+15 minutes")), "active_time" => date("Y-m-d H:i:s")];
      $query = $this->db->table("list_session_token")->where("session_token", $session)->update($update);
      if (!$query) {
        dd($query);
      }
    }
  }

  protected function check_login_user($admin_id)
  {
    $response = \create_response();
    $sql = "SELECT * FROM `list_session_token` "
      .  "WHERE `admin_id` = ? "
      .  "ORDER BY `session_id` DESC "
      .  "LIMIT 1;";
    $query = $this->db->query($sql, [$admin_id]);
    if ($query) {
      // dd($this->db->getLastQuery());
      if ($query->resultID->num_rows == 1) {
        $data = $query->getRow();
        // dd($data);
        if ($data->is_login == 1) {
          $expire_time = $data->expire_time;
          $now = date("Y-m-d H:i:s");
          if ($expire_time >= $now) {
            $response->message = "Saat ini user sedang digunakan!";
          } else {
            $response->success = TRUE;
          }
        } else {
          $response->success = TRUE;
        }
      } else {
        $response->success = TRUE;
      }
    } else {
      $response->message = "Query check login user failed!";
    }
    return $response;
  }

  public function check_session_token()
  {
    $response = \create_response();
    if ($this->session->is_login === TRUE) {
      $session_token = $this->session->token;
      $query = $this->db->table("list_session_token")->where("session_token", $session_token)->get();
      if ($query) {
        if ($query->resultID->num_rows == 1) {
          $data = $query->getRow();
          $now = date("Y-m-d H:i:s");
          if ($data->expire_time < $now) {
            $response->message = "Session is expired!";
          } else {
            $this->update_session_token();
            $response->success = TRUE;
          }
        } else {
          $response->message = "Session is not valid!";
        }
      } else {
        $response->message = "Query failed!";
      }
    } else {
      $response->success = TRUE;
    }
    return $response;
  }

  protected function generate_random_string($size = FALSE)
  {
    $characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $length = strlen($characters);
    $random_string = "";
    if ($size === FALSE or !is_integer($size)) {
      $size = 26;
    }
    for ($i = 0; $i < $size; $i++) {
      $random_string .= $characters[rand(0, $length - 1)];
    }
    return $random_string;
  }

  public function check_validate_user($rules, $user)
  {
    $response = \create_response();
    $division_name = $this->session->division_name;
    $access_level = $this->session->access_level;
    for ($i = 0; $i < count($user); $i++) {
      $array = \explode("|", $user[$i]);
      if ($rules == "WHITE_LIST") {
        if (end($array) == 0) {
          if ($division_name == $array[0] && $access_level == $array[1]) {
            $response->success = TRUE;
            break;
          }
        } else {
          if ($division_name == $array[0] && $access_level >= $array[1]) {
            $response->success = TRUE;
            break;
          }
        }
      } else if ($rules == "BLACK_LIST") {
        if (end($array) == 0) {
          if ($division_name == $array[0] && $access_level != $array[1]) {
            $response->success = TRUE;
          }
        } else {
          if ($division_name == $array[0] && $access_level <= $array[1]) {
            $response->success = TRUE;
          }
        }
      }
    }
    if ($response->success === FALSE) {
      $response->message = "Maaf akun anda tidak bisa mengakses halaman ini!";
    }
    return $response;
  }

  public function check_unique($table, $column, $value)
  {
    $query = $this->db->table($table)->where($column, $value)->get();
    if ($query) {
      // dd($query->resultID->num_rows);
      if ($query->resultID->num_rows == 1) {
        return TRUE;
      } else {
        return FALSE;
      }
    } else {
      return "Query failed!";
    }
  }

  public function get_currently_admin_online()
  {
    $sql = "SELECT la.`admin_name`, lst.`active_time`, la.`admin_id` "
      .  "FROM `list_session_token` lst "
      .  "JOIN `list_admin` la ON la.`admin_id` = lst.`admin_id` "
      .  "WHERE lst.`active_time` > (NOW() - INTERVAL 15 MINUTE) AND lst.`is_login` = 1 "
      .  "GROUP BY lst.`admin_id`;";
    $query = $this->query($sql);
    if ($query) {
      return $query->getResult();
    } else {
      return FALSE;
    }
  }

  public function get_admin_detail($id)
  {
    return $this->db->table("list_admin la")->join("list_access_control lac", "lac.`admin_tier_id` = la.`admin_tierId`")
      ->join("`list_division` ld", "ld.`division_id` = lac.`access_divisionId`")->where("la.`admin_id`", $id)->get()->getRow();
  }

  public function get_access_listed()
  {
    $where_sql = "";
    if ($this->session->access_level < 100) {
      $where_sql = "WHERE lac.`access_level` < 100";
    }
    $sql = "SELECT * FROM `list_access_control` lac "
      .  "JOIN `list_division` ld ON ld.`division_id` = lac.`access_divisionId` "
      .  "$where_sql";
    $query = $this->db->query($sql);
    if ($query) {
      return $query->getResult();
    } else {
      return FALSE;
    }
  }

  public function edit_admin($input, $file)
  {
    $response = \create_response();
    $error = FALSE;
    $data = [
      "admin_name" => $input->admin_name,
      "admin_email" => $input->admin_email,
      "admin_phone" => $input->admin_phone,
      "admin_tierId" => $input->admin_tierId
    ];
    if ($file != NULL) {
      $change_photo = $this->change_photo_admin($file, decrypt_url($input->admin_id));
      if ($change_photo->success === TRUE) {
        $data["admin_photo"] = $change_photo->file_path;
      } else {
        $error = TRUE;
        $response->message = $change_photo->message;
      }
    }
    if (isset($input->password) && $input->password != "") {
      $data["admin_password"] = \password_hash($input->password, \PASSWORD_DEFAULT);
    }
    if ($error === FALSE) {
      $query = $this->db->table("list_admin")->update($data, ["admin_id" => \decrypt_url($input->admin_id)]);
      if ($query) {
        $response->success = TRUE;
        $response->message = "Sukses update data admin!";
      } else {
        $response->message = "Query failed!";
      }
    }
    return $response;
  }

  protected function change_photo_admin($file, $id)
  {
    $response = \create_response();
    // dd($file);
    $error = FALSE;
    $check_last_photo = $this->check_last_file("list_admin", "admin_id", $id, "admin_photo");
    if ($check_last_photo->success === TRUE) {
      if ($check_last_photo->found === TRUE) {
        $last_file = explode("/", $check_last_photo->last_file);
        if (!\unlink("./image/admin_photo/" . end($last_file))) {
          $response->message = "Failed deleted old photo!";
          $error = TRUE;
        }
      }
    } else {
      $error = TRUE;
      $response->message = $check_last_photo->message;
    }

    if ($error === FALSE) {
      $upload_photo = $this->upload_file($file, "list_admin", "admin_photo", "image/admin_photo/");
      if ($upload_photo->success === TRUE) {
        $response->success = TRUE;
        $response->file_path = $upload_photo->file_path;
      } else {
        $response->message = $upload_photo->message;
      }
    }

    return $response;
  }

  protected function check_last_file($table, $column, $value, $column_search)
  {
    $response = \create_response();
    $query = $this->db->table($table)->where($column, $value)->get();
    if ($query) {
      $data = $query->getRow();
      if ($data->{$column_search} != "" && $data->{$column_search} != NULL) {
        $response->found = TRUE;
        $response->last_file = $data->{$column_search};
      } else {
        $response->found = FALSE;
      }
      $response->success = TRUE;
    } else {
      $response->message = "Query check last file failed!";
    }
    return $response;
  }

  protected function upload_file($file, $table, $column_name, $path)
  {
    $response = create_response();
    // dd($file);
    do {
      $name_file = $file->getRandomName();
      $file_path = \base_url("$path/$name_file");
      $check_name = $this->check_unique($table, $column_name, $file_path);
    } while ($check_name === TRUE);
    if ($file->move("$path", $name_file)) {
      $response->success = TRUE;
      $response->file_path = $file_path;
    } else {
      $response->message = "Failed move file!";
    }
    return $response;
  }

  public function get_admin_login()
  {
    $admin_id = \decrypt_url($this->session->admin_id);
    $query = $this->db->table("list_admin")->select("admin_name, admin_photo")->where("admin_id", $admin_id)->get();
    return $query->getRow();
  }
}
