<?php

namespace App\Models;

use App\Models\Core_model;

class Auth_model extends Core_model
{
  public function check_login($email, $password)
  {
    $response = \create_response();
    $check = $this->check_login_by_email($email);
    // dd($check);
    if ($check->success === TRUE) {
      $data = $check->data;
      if (password_verify($password, $data->admin_password)) {
        if ($data->admin_statusId == 1) {
          $check_user_login = $this->check_login_user($data->admin_id);
          // dd($check_user_login);
          if ($check_user_login->success === TRUE) {
            $create_session = $this->create_session_token($data->admin_id);
            if ($create_session->success === TRUE) {
              $response->success = TRUE;
              $set_session = [
                "admin_id" => encrypt_url($data->admin_id),
                "access_level" => $data->access_level,
                "division_name" => $data->division_name,
                "admin_tierId" => $data->admin_tierId,
                "admin_name" => $data->admin_name,
                "token" => $create_session->token,
                "image_admin" => $data->admin_photo,
                "is_login" => TRUE
              ];
              $this->session->set($set_session);
            } else {
              $response->message = $create_session->message;
            }
          } else {
            $response->message = $check_user_login->message;
          }
        } else {
          $response->message = $data->admin_statusId == 3 ? "Maaf akun anda dibanned!" : "Maaf, akun anda belum diaktifkan oleh administrator!";
        }
      } else {
        $response->message = "Password salah!";
      }
    } else {
      $response->message = $check->message;
    }
    return $response;
  }

  private function check_login_by_email($email)
  {
    $response = \create_response();
    $query =  $this->db->table("list_admin la")->where("admin_email", $email)->join("list_access_control lac", "la.`admin_tierId` = lac.`admin_tier_id`")
      ->join("list_division ld", "ld.`division_id` = lac.`access_divisionId`")->get();
    if ($query) {
      if ($query->resultID->num_rows == 1) {
        $response->success = TRUE;
        $response->data = $query->getRow();
      } else {
        $response->message = "Akun tidak ditemukan!";
      }
    } else {
      $response->message = "Query check login by email failed!";
    }
    return $response;
  }

  private function create_session_token($admin_id)
  {
    $response = \create_response();
    do {
      $session_token = $this->generate_random_string();
      $query = $this->db->table("list_session_token")->where("session_token", $session_token)->get();
      if ($query) {
        if ($query->resultID->num_rows == 0) {
          $accept_token = TRUE;
        } else {
          $accept_token = FALSE;
        }
      } else {
        $accept_token = FALSE;
        $response->message = "Query created session token failed!";
        break;
      }
    } while ($accept_token === FALSE);

    if ($accept_token === TRUE) {
      $data = [
        "session_token" => $session_token,
        "admin_id" => $admin_id,
        "active_time" => date("Y-m-d H:i:s"),
        "expire_time" => date("Y-m-d H:i:s", \strtotime("+15 minutes")),
        "is_login" => 1
      ];
      $query2 = $this->db->table("list_session_token")->insert($data);
      if ($query2) {
        $response->token = $session_token;
        $response->success = TRUE;
      } else {
        $response->message = "Query 2 Session Token failed!";
      }
    }

    return $response;
  }

  public function logout()
  {
    $data = ["is_login" => 0];
    $where = ["session_token" => $this->session->token];
    $this->db->table("list_session_token")->where($where)->update($data);
  }
}
