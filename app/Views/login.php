<?= $this->extend("layout/admin/template") ?>

<?= $this->section("body"); ?>
<?php session_destroy(); ?>
<div class="login-box">
  <div class="login-logo">
    <a href="../../index2.html"><b>Admin</b>LTE</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form action="<?= base_url("process_login") ?>" method="post">
        <?= csrf_field(); ?>
        <?= ($validation->hasError("email")) ? "<small class='text-danger'>".$validation->getError("email")."</small>" : FALSE; ?>
        <div class="input-group mb-3">
          <input type="text" class="form-control <?= ($validation->hasError("email")) ? "is-invalid" : ""; ?>" id="emailUser" placeholder="Email" name="email" value="<?= old("email") ?>">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <?= ($validation->hasError("password")) ? "<small class='text-danger'>".$validation->getError("password")."</small>" : FALSE; ?>
        <div class="input-group mb-3">
          <input type="password" class="form-control <?= ($validation->hasError("password")) ? "is-invalid" : ""; ?>" placeholder="Password" name="password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <p class="mb-1">
        <a href="forgot-password.html">I forgot my password</a>
      </p>
      <p class="mb-0">
        <a href="register.html" class="text-center">Register a new membership</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<?= $this->endSection(); ?>