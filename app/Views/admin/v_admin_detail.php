<?= $this->extend("layout/admin/template_admin"); ?>

<?= $this->section("content_admin"); ?>
<div class="content-header">
  <div class="container-fluid">
    <?= $breadcrumb; ?>
  </div>
</div>
<div class="container-fluid">
  <div class="card">
    <div class="card-body">
      <button class="btn btn-info btn-sm d-block ml-auto mb-3" data-toggle="modal" data-target="#editModal"><i class="fas fa-edit"></i></button>
      <div class="row">
        <div class="col-md-5">
          <img src="<?= $admin_detail->admin_photo ?>" alt="Image Profile" class="img-fluid w-100 h-auto" onerror="onErrorImage(this)">
        </div>
        <div class="col-md-7">
          <div class="table-responsive">
            <table class="table">
              <tr>
                <td>Name</td>
                <td>: <?= $admin_detail->admin_name; ?></td>
              </tr>
              <tr>
                <td>Email</td>
                <td>: <?= $admin_detail->admin_email; ?></td>
              </tr>
              <tr>
                <td>Phone</td>
                <td>: <?= $admin_detail->admin_phone; ?></td>
              </tr>
              <tr>
                <td>Access Level</td>
                <td>: <?= $admin_detail->access_levelName; ?></td>
              </tr>
              <tr>
                <td>Division</td>
                <td>: <?= $admin_detail->division_name; ?></td>
              </tr>
              <tr>
                <td>Last Login</td>
                <td>: <?= $admin_detail->admin_lastLogin != NULL ? date("d M Y H:i", strtotime($admin_detail->admin_lastLogin)) : "-"; ?></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- The Modal -->
<div class="modal fade" id="editModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Profile</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <?= form_open_multipart("update_admin") ?>
          <input type="hidden" name="admin_id" value="<?= encrypt_url($admin_detail->admin_id); ?>">
          <div class="form-group">
            <label>Name</label>
            <input type="text" name="admin_name" class="form-control <?= ($validation->hasError("admin_name")) ? "is-invalid" : ""; ?>" value="<?= $admin_detail->admin_name; ?>">
            <?= ($validation->hasError("admin_name")) ? "<small class='text-danger'>".$validation->getError("admin_name")."</small>" : FALSE; ?>
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="email" name="admin_email" class="form-control <?= ($validation->hasError("admin_email")) ? "is-invalid" : ""; ?>" value="<?= $admin_detail->admin_email; ?>">
            <?= ($validation->hasError("admin_email")) ? "<small class='text-danger'>".$validation->getError("admin_email")."</small>" : FALSE; ?>
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" name="password" class="form-control <?= ($validation->hasError("password")) ? "is-invalid" : ""; ?>" placeholder="If not changes, just empty it">
            <?= ($validation->hasError("password")) ? "<small class='text-danger'>".$validation->getError("password")."</small>" : FALSE; ?>
          </div>
          <div class="form-group">
            <label>Re-Password</label>
            <input type="password" name="password2" class="form-control <?= ($validation->hasError("password2")) ? "is-invalid" : ""; ?>" placeholder="If not changes, just empty it">
            <?= ($validation->hasError("password2")) ? "<small class='text-danger'>".$validation->getError("password2")."</small>" : FALSE; ?>
          </div>
          <div class="form-group">
            <label>Phone</label>
            <input type="text" name="admin_phone" class="form-control <?= ($validation->hasError("admin_phone")) ? "is-invalid" : ""; ?>" value="<?= $admin_detail->admin_phone; ?>">
            <?= ($validation->hasError("admin_phone")) ? "<small class='text-danger'>".$validation->getError("admin_phone")."</small>" : FALSE; ?>
          </div>
          <div class="form-group">
            <label>Access Level</label>
            <select name="admin_tierId" class="form-control <?= ($validation->hasError("admin_tierId")) ? "is-invalid" : ""; ?>">
              <?php 
                foreach ($list_access as $item) {
                  if ($item->admin_tier_id == $admin_detail->admin_tierId) {
                    echo "<option selected value='$item->admin_tier_id'>$item->access_levelName - $item->division_name</option>";
                  } else {
                    echo "<option value='$item->admin_tier_id'>$item->access_levelName - $item->division_name</option>";
                  }
                }
              ?>
            </select>
            <?= ($validation->hasError("admin_tierId")) ? "<small class='text-danger'>".$validation->getError("admin_tierId")."</small>" : FALSE; ?>
          </div>
          <div class="form-group">
            <label>Admin Photo</label>
            <input type="file" name="admin_photo" class="form-control <?= ($validation->hasError("admin_photo") ? "is-invalid" : "") ?>">
            <?= ($validation->hasError("admin_photo") ? "<small class='text-danger'>".$validation->getError("admin_photo")."</small>" : FALSE) ?>
          </div>
          <input type="submit" class="btn btn-success btn-sm" value="Simpan Perubahan">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?= $this->endSection(); ?>