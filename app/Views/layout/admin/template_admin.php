<?= $this->extend("layout/admin/template") ?>

<?= $this->section("body") ?>
<div class="wrapper">

  <?= $this->include("layout/admin/template_header"); ?>

  <?= $this->include("layout/admin/template_sidenav"); ?>

  <div class="content-wrapper">
      <?= $this->renderSection("content_admin"); ?>
  </div>

  <div class="modal fade" id="isLogout">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Logout</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <h4>Anda yakin ingin logout?</h4>
      </div>
      <div class="modal-footer">
        <a href="<?= base_url("logout") ?>" class="btn btn-danger btn-sm">Logout</a>
        <button type="button" class="btn btn-success btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

  <aside class="control-sidebar control-sidebar-dark">
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>

  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer>
</div>
<?= $this->endSection(); ?>