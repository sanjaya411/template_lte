<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title><?= $title ?></title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="/plugins/sweetalert2/sweetalert2.min.css">
  <link rel="stylesheet" href="/plugins/chart.js/Chart.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="<?= $title != "Login" ? "hold-transition sidebar-mini" : "hold-transition login-page" ?>">

  <script src="/plugins/jquery/jquery.min.js"></script>
  <script src="/plugins/sweetalert2/sweetalert2.min.js"></script>
  <script src="/plugins/chart.js/Chart.js"></script>

  <?= $this->renderSection("menu_admin") ?>
  <?= $this->renderSection("body") ?>

  <script src="/js/app.js"></script>
  <?= session()->getFlashdata("pesan") ?>
  <script src="/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="/js/adminlte.min.js"></script>
</body>

</html>