<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Auth_model;

class BaseController extends Controller
{
	protected $helpers = ["session", "form"];
	protected $core_model;

	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
	{
		parent::initController($request, $response, $logger);
		\helper($this->helpers);
		$this->core_model = new Auth_model();
	}

	protected function htaccess($rules, $user, $is_ajax = TRUE)
	{
		$response = \create_response();
		$check_validate = $this->core_model->check_validate_user($rules, $user);
		if ($check_validate->success === FALSE) {
			if ($is_ajax == TRUE) {
				$response->data = [
					"success" => 403,
					"link" => base_url("forbidden")
				];
			}
		} else {
			$response->success = TRUE;
		}
		return $response;
	}

	protected function draw_breadcrumb($link, $title, $delete = FALSE)
	{
		if ($delete === TRUE) {
			$breadcrumb = [[
				"title" => $title,
				"link" => $link
			]];
			session()->remove("breadcrumb");
			session()->set("breadcrumb", $breadcrumb);
		} else {
			$breadcrumb = [];
			foreach (session()->breadcrumb as $item) {
				if ($item["title"] != $title) {
					array_push($breadcrumb, ["title" => $item["title"], "link" => $item["link"]]);
				}
			}
			array_push($breadcrumb, ["title" => $title, "link" => $link]);
			session()->remove("breadcrumb");
			session()->set("breadcrumb", $breadcrumb);
		}
		$data = session()->breadcrumb;
		$last = \count($data) - 1;
		$html = "<nav aria-label='breadcrumb'>";
		$html .= "<ol class='breadcrumb'>";
		for ($i = 0; $i < \count($data); $i++) {
			if ($i != $last) {
				$html .= "<lu class='breadcrumb-item'><a href='" . $data[$i]["link"] . "'>" . $data[$i]["title"] . "</a></lu>";
			} else {
				$html .= "<lu class='breadcrumb-item active'>" . $data[$i]["title"] . "</lu>";
			}
		}
		$html .= "</ol>";
		$html .= "</nav>";
		return $html;
	}

	public function view_forbidden()
	{
		$data["title"] = "Forbidden";
		return \view("v_forbidden", $data);
	}
}
