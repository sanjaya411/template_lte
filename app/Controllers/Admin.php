<?php

namespace App\Controllers;

class Admin extends BaseController
{
	public function view_dashboard()
	{
		$data["title"] = "Dashboard";
		$data["admin_online"] = $this->core_model->get_currently_admin_online();
		$data["admin_login"] = $this->core_model->get_admin_login();
		$data["breadcrumb"] = $this->draw_breadcrumb(base_url("dashboard"), "Dashboard", TRUE);
		return view('admin/v_dashboard', $data);
	}

	public function view_admin_detail()
	{
		$id = $this->request->getGet("id");
		$data["title"] = "Admin Detail";
		$data["breadcrumb"] = $this->draw_breadcrumb(base_url("admin_detail?id=$id"), "Admin Detail");
		$data["admin_detail"] = $this->core_model->get_admin_detail(decrypt_url($id));
		$data["list_access"] = $this->core_model->get_access_listed();
		$data["validation"] = \Config\Services::validation();
		$data["admin_login"] = $this->core_model->get_admin_login();
		return view("admin/v_admin_detail", $data);
	}

	public function validate_admin_edit()
	{
		$validate = [
			"admin_id" => [
				"label" => "Admin Id",
				"rules" => "required|alpha_numeric",
				"errors" => [
					"required" => "{field} Wajib di isi!",
					"alpha_numeric" => "{field} Tidak valid!"
				]
			],
			"admin_name" => [
				"label" => "Nama Admin",
				"rules" => "required|alpha_space",
				"errors" => [
					"required" => "{field} Wajib di isi!",
					"alpha_space" => "{field} Hanya boleh huruf!"
				]
			],
			"admin_email" => [
				"label" => "Email",
				"rules" => "required|valid_email",
				"errors" => [
					"required" => "{field} Wajib di isi!",
					"valid_email" => "{field} Format tidak benar!"
				]
			],
			"password" => [
				"label" => "Password",
				"rules" => "matches[password2]",
				"errors" => [
					"matches" => "{field} Tidak sesuai!"
				]
			],
			"password2" => [
				"label" => "Password",
				"rules" => "matches[password]",
				"errors" => [
					"matches" => "{field} Tidak sesuai!"
				]
			],
			"admin_phone" => [
				"label" => "Nomor Hp",
				"rules" => "required|numeric",
				"errors" => [
					"required" => "{field} Wajib di isi!",
					"numeric" => "{field} Hanya boleh angka!"
				]
			],
			"admin_tierId" => [
				"label" => "Access Level",
				"rules" => "required|numeric",
				"errors" => [
					"required" => "{field} Wajib di isi!",
					"numeric" => "{field} Data tidak valid!"
				]
			],
			"admin_photo" => [
				"rules" => "max_size[admin_photo,1024]|is_image[admin_photo]|mime_in[admin_photo,image/jpg,image/jpeg,image/png]",
				"errors" => [
					"max_size" => "Ukuran gambar maksimal 1MB!",
					"is_image" => "Yang anda pilih bukan gambar",
					"mime_in" => "Yang anda pilih bukan gambar"
				]
			]
		];

		if ($this->validate($validate)) {
			$input = (object)$this->request->getPost();
			$id = $this->request->getPost("admin_id");
			$file = NULL;
			if (isset($_FILES["admin_photo"]) && $_FILES["admin_photo"]["name"] != "") {
				$file = $this->request->getFile("admin_photo");
			}
			$edit_admin = $this->core_model->edit_admin($input, $file);
			if ($edit_admin->success === TRUE) {
				session()->setFlashdata("pesan", "<script>sweet('success', 'Sukses!', '$edit_admin->message')</script>");
			} else {
				session()->setFlashdata("pesan", "<script>sweet('error', 'Gagal!', '$edit_admin->message')</script>");
			}
			return redirect()->to("/admin_detail?id=$id");
		} else {
			$id = $this->request->getPost("admin_id");
			session()->setFlashdata("pesan", "<script>sweet('error', 'Gagal!', 'Data yang di input tidak sesuai!')</script>");
			return \redirect()->to("/admin_detail?id=$id")->withInput();
		}
	}
}