<?php

namespace App\Controllers;

use App\Models\Auth_model;

class Auth extends BaseController
{
	public function view_login_page()
	{
		$data["title"] = "Login";
		$data["validation"] = \Config\Services::validation();
		return view('login', $data);
	}

	public function validation_login()
	{
		$validation = [
			"email" => [
				"label" => "Email",
				"rules" => "required|valid_email",
				"errors" => [
					"required" => "Wajib masukan email!",
					"valid_email" => "Format email tidak benar!"
				]
			],
			"password" => [
				"label" => "Label",
				"rules" => "required|alpha_numeric_space",
				"errors" => [
					"required" => "Password tidak boleh kosong!",
					"alpha_numeric_space" => "Data password tidak valid!"
				]
			]
		];
		if ($this->validate($validation)) {
			$email = $this->request->getPost("email");
			$password = $this->request->getPost("password");
			$check = $this->model->check_login($email, $password);
			if ($check->success === TRUE) {
				return redirect()->to("/dashboard");
			} else {
				session()->setFlashdata("pesan", "<script>sweet('error', 'Gagal!', '$check->message')</script>");
				return redirect()->to("/admin")->withInput();
			}
		} else {
			session()->setFlashdata("pesan", "<script>sweet('error', 'Gagal!', 'Data tidak lengkap!')</script>");
			return redirect()->to("/admin")->withInput();
		}
	}

	public function process_logout()
	{
		$this->model->logout();
		session()->setFlashdata("pesan", "<script>sweet('success', 'Sukses!', 'Anda berhasil logout!')</script>");
		return \redirect()->to("/admin");
	}
}